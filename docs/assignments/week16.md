# 4. Networking and communications
The goal of this week was to create a network application using the available hardware (Olimex, Arduino and the ATtiny412), however, due to the limitations with wireless interfaces, I created a simple application that allows the communication betwen the computer, an Arduino UNO and the ATtiny412 via serial port.
## Used hardware and tools.
For this application one Arduino UNO and one ATtiny 412 were used, moreover, I used two leds, two resistors (1k) and the Arduino IDE, which was the tool for programming the devices.

## Followed steps.

I wrote pieces of code for the Arduino and the ATtiny412, both of them would act as receivers. The Arduino was connected to the computer using the serial port, and this one shared the serial port with the ATtiny using ports Tx and Rx (0 and 1). The idea behind the porject was as follows: Two addresses (A and B) were assigned to the ATtiny412 and Ardiuino, respectively, and two leds (red and blue) were connected to ports 9 an 10 of the Arduino using two resistors (1 k each one). Two bytes (two characters) were sent to the boards in each transmission using the serial monitor of the Arduino application, the first one contains the address of the intended recipient, and the second one the action ('b'-turns on the blue led, 'r'-turns on the red leds and 'o'- turns off all leds). The picture below shows the mounted architecture.

## Architecture.
The figure shows the way the boards and computer were connected.
![](../images/uploaded_images/IMG_20200818_171014.jpg ) 

Below the codes for both the Arduino UNO and the ATtiny412 are displayed.


## Code for Arduino.

```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3); // RX, TX

const char addr = 'B'; // network address
const char red = 'r';
const char blue = 'b';
const char off = 'o';
bool flag = false;
const int REDled =  10; // the number of the LED pin
const int BLUEled = 9;

void setup() {
  mySerial.begin(9600);
  pinMode(REDled, OUTPUT);
  //pinMode(BLUEled, OUTPUT);
  pinMode(3, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {
    
    char receivedByte = mySerial.read();
    if (flag == true){
      if(receivedByte == red){
        digitalWrite(REDled, HIGH);
      }
      if(receivedByte == blue){
        digitalWrite(BLUEled, HIGH);
      }
      if(receivedByte == off){
        digitalWrite(REDled, LOW);
        //digitalWrite(BLUEled, LOW);
      }
      flag = false;
    }
    
    if (receivedByte == addr) {      
      flag = true;
    }
  }
}

```
## Code for the ATtiny412.

```
#include <SoftwareSerial.h>
SoftwareSerial mySerial(0, 1); // RX, TX

const char addr = 'A'; // network address
const char on = 'r';
const char off = 'o';
bool flag = false;
const int pinled =  1; // the number of the LED pin

void setup() {
  mySerial.begin(9600);
  pinMode(led, OUTPUT);
  //pinMode(BLUEled, OUTPUT);
  pinMode(1, INPUT);
}

void loop() {
  if (mySerial.available() > 0) {
    
    char receivedByte = mySerial.read();
    if (flag == true){
      if(receivedByte == on){
        digitalWrite(pinled, HIGH);
      }
      if(receivedByte == blue){
        digitalWrite(pinled, LOW);
      }
      flag = false;
    }
    
    if (receivedByte == addr) {      
      flag = true;
    }
  }
}



```
### Video

In addition to the previous description, here I attached  a small video showing the functioning of the proposed scheme.



<iframe src="https://player.vimeo.com/video/449019963" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

