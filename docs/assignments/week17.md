# 5. Group assignment week 2
# Tasks
The main goal for this week was to create a simple code for arduino and create as many graphic interfaces as possibles in the computer using different programming languages and environments, so that we can control the Arduino through such interfaces. This group is also formed by Nelson Mayedo, Osmel Martinez and henrique Hilleshein. The links to the other group assignments are shown below.
https://nelsongi.gitlab.io/nelsonproject/assignments/week19/
https://henriquehilleshein.gitlab.io/olimex_pushbutton_led/assignments/week04/


# Project
The implemented idea was the control of a servo motor using Arduino, such that it rotates the shaft 90 degrees on each direction depending on the pressed button. 

# Tools 
For this task we used an Arduino UNO, a servo motor, three wires to connect the servo and the Arduino, and a cable for the serial connection to the computer. Moreover, we used Python, Processing and Matlab in order to create simple graphic interfaces. The wires of the servo were connected to the ports 5V and GND of the Arduino, and the third one to the port 2 for signal reception. The created graphic interfaces have two buttons (Open and Close) that rotates the shaft in one or other direction when pressed. The figures below show the Arduino and servo motor used for the application. 

![](../images/uploaded_images/image.png)
![](../images/uploaded_images/MOT2263.jpg)


# Code
Below we attach the code used for the Arduino.

```
#include <Servo.h>
Servo servoMotor;
int servPin = 2;
int pulseMin = 1000;
int pulseMax = 2000;

void setup() {
 servoMotor.attach(servPin,pulseMin,pulseMax);
 Serial.begin(9600);
 }

void loop() {

  if(Serial.available()){

    char val = Serial.read();
    if(val == 'u'){
       servoMotor.write(180);   // if 'u' received move motor 180 degree
       delay(200); 
    }
    if(val == 'd'){
       servoMotor.write(0);  // if 'd' is received move motor 0 degree
    }
  }
}
```

Here the code for Processing and the interface.

```
import controlP5.*;
import processing.serial.*;

Serial port;

ControlP5 cp5;

void setup(){

  size(300,400);
  printArray(Serial.list());
  port = new Serial(this,"COM4",9600);

  cp5 = new ControlP5(this);

  cp5.addButton("Open")
  .setPosition(100, 50)
  .setSize(100, 80)
  ;
  cp5.addButton("Close")
  .setPosition(100, 250)
  .setSize(100, 80)
  ;

}
    void draw(){
      background(150,0,150);
    }

// Adding functionality to the bottons   
// When a button is pressed then some character shoudl be sent to Arduino
 void Open(){
   port.write('u');
 }
 void Close(){
 port.write('d');
 }
```
![](../images/uploaded_images/ProcessingPhoto.jpg)

The code for Python and the graphic interface.

```
import serial 
import tkinter


arduinoData = serial.Serial('com4',9600)

def servoOn():
    arduinoData.write(('u').encode())
def servoOFF():
    arduinoData.write(('d').encode())

servoControlWindow = tkinter.Tk()
servoControlWindow.title(" Servo Control")
servoControlWindow.configure(background="#94d42b")
servoControlWindow.geometry("200x150")

btn = tkinter.Button(servoControlWindow,width=10,height=2, text="Open", command = servoOn)
btn1 = tkinter.Button(servoControlWindow,width=10,height=2, text="Close", command = servoOFF)

btn.grid(row=0,column=3)
btn1 = tkinter.Button(servoControlWindow,width=10,height=2, text="Close", command = servoOFF)

btn.grid(row=0,column=3)
btn1.grid(row=2,column=3)
servoControlWindow.mainloop()
input("Press enter to exit")

```

![](../images/uploaded_images/pythonPhoto.jpg)

Finally, the code and interface in Matlab using GUIDE.

```
function varargout = servo_1(varargin)
% SERVO_1 MATLAB code for servo_1.fig
%      SERVO_1, by itself, creates a new SERVO_1 or raises the existing
%      singleton*.
%
%      H = SERVO_1 returns the handle to a new SERVO_1 or the handle to
%      the existing singleton*.
%
%      SERVO_1('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SERVO_1.M with the given input arguments.
%
%      SERVO_1('Property','Value',...) creates a new SERVO_1 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before servo_1_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to servo_1_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help servo_1

% Last Modified by GUIDE v2.5 09-May-2020 00:56:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @servo_1_OpeningFcn, ...
                   'gui_OutputFcn',  @servo_1_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before servo_1 is made visible.
function servo_1_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to servo_1 (see VARARGIN)

% Choose default command line output for servo_1
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes servo_1 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = servo_1_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
clear all;
global x;
x=serial('COM7','BAUD', 9600); % Make sure the baud rate and COM port is  
% same as in Arduino IDE
fopen(x);

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global x;
fprintf(x,'d');

% Hint: get(hObject,'Value') returns toggle state of pushbutton1


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global x;
fprintf(x,'u');

```

![](../images/uploaded_images/servo.jpg)


# Video

In addition to the previous explanations, pictures and provided code, here, we show two videos which contain the implementation of the project using the different programming languages. 
### Video for Python.


<iframe src="https://player.vimeo.com/video/401971330" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

### Video for Matlab.


<iframe src="https://player.vimeo.com/video/416648839" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

### Video for Processing.

<iframe src="https://player.vimeo.com/video/416657151" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

