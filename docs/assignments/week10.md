# 2. Interface and application programming

## Project overview.
The main goal for this week was to connect the Arduino UNO and Attiny412 to the computer or to a smartphone and interact with it via serial communications. It was also required to display the data and send instructions through a graphic interface generated using any available tool such as Processing. The link for the group assignment this week can be found [here](https://dechevar19.gitlab.io/gitlab-project/assignments/week17/).


## Proposed setup and steps for the Arduino.

I wrote pieces of code for Arduino and Processing that allowed me to connect the Arduino UNO to the laptop via serial port (Serial communication protocol) and display data in an inteface in Windows. For this purpose I used a sensor ('Adafruit SGP30') which is used for measuring the quality of the air, returning data of the amount of CO or CO2 in parts per million (ppm). More information on this sensor can be found [here](https://cdn-learn.adafruit.com/downloads/pdf/adafruit-sgp30-gas-tvoc-eco2-mox-sensor.p). The sensor was connected to the ports GND, VCC, A4 and A5 of the Arduino and a picture of is shown below.
![](../images/week_10/arduino_sensor.jpg)
The Arduino reads the data from the sensor and compares with a threshold (fixed to 2000 ppm) and sends the message "Normal level of CO2 + data" if the reading is below the threshold or "Dangerous level of CO2 + data" if above, In addition to this, a diode is also activated in the Arduino whenever the values of CO2 are above the threshold. The communication is through the serial port as mentioned before. Then, the computer reads the data and prints them on the visual interface. For determinig the threshold we used the value 2000 ppm since above this value the CO2 concentration can cause headaches, sleepiness etc. (See more information in https://www.kane.co.uk/knowledge-centre/what-are-safe-levels-of-co-and-co2-in-ro).The arduino and Processing environments, and the graphic interface displaying different values are shown below.
![](../images/week_10/arduino_ide.jpg)
![](../images/week_10/processing.jpg)
![](../images/week_10/processing_1.jpg)


## Communication.
Information about the serial protocol (data frame structure, synchronization, etc) and its functioning can be found [here](https://circuitdigest.com/tutorial/serial-communication-protocols#:~:text=Now%20if%20the%20data%20is,communication%20channel%20in%20Serial%20Communication.)).
## Code for Arduino.
Here, the code used for Arduino is shown and commented. The file can be found [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/arduino.ino).

```
#include <Wire.h>            // Including the required libraries
#include "Adafruit_SGP30.h"   // Including the required libraries

Adafruit_SGP30 sgp;           // 
int ledPin = 13;              // Declare the variable ledpin as an integer and assign pin 13

void setup() {
Serial.begin(9600);             //Initialize the serial port to 9600 baudios
sgp.begin();                    // Initialize the sensor
pinMode(ledPin, OUTPUT);        // Declare the pin 13 as output
}

void loop() {                   // Loop
sgp.IAQmeasure();               // Reads the data from the sensor
if (sgp.eCO2 >= 2000){          // compares the reading to the threshold
digitalWrite(ledPin, HIGH);     //Turns on the led
Serial.print("Dangerous level of CO2 (ppm): "); Serial.println(sgp.eCO2);   // sends the message through the serial port
delay(1000);                                // Introduces  1s delay between readings

}
else{
Serial.print("Normal level of CO2 (ppm): "); Serial.println(sgp.eCO2);     // sends the message through the serial port
delay(1000);     // Introduces  1s delay between readings
digitalWrite(ledPin, LOW);    //Turns off the led
}
}
```
## General structure of a processing code.
Processing code consists of two main parts, setup and draw blocks. Setup block runs once when the code is executed, and the draw blocks runs continuously. The main idea behind Processing is, what you write within the draw block will be executed 60 times per second from top to bottom, until your program terminates. As mentioned before, the setup block is executed once and is used for making configuration or running things that will executed once, for instance, loading images or sounds. Functions such as background(), size(), fill() and stroke() can be used within setup(). The block draw() is the main part of the code, here we can write all the shapes and objects. For doing so, we can used commands such as point(), line(), rect(), triangle() and quad(), etc. Keyboard and mouse interactions in Processing are very easy and straightforward. There are methods you can call for each event, and what you write inside will be executed once when the event occurs. For animations variables can be used as counters, so that they can be incremented/decremented (modifying the coordinates) and hence, varying the positions of a given shape in the visual window. These are basic insigths of the preocessing language, for more information, please refer to this [link](https://processing.org/).

## Code for Processing.
The code below is the one used for generating the simple interface using Processing, the comments in the code allows to understand what is each line used for. The code is also [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/processing.pde). Basically, I declare the required variables at the beginning of the code. Then, the setup is declared, this function is run once, when the program starts. This is used to define initial enviroment properties such as screen size, etc. There, I also initialize the serial port to the rate 9600 baud/s and declare the font size. The 'draw loop' is constantly being executed, doing all the actions contained in it (drawing the rectangles, filling with different colors and printing the data from the serial port). 

```
import processing.serial.*;
Serial myPort;  // Create object from Serial class
String val;     // Data received from the serial port
PFont variable_font; 

void setup()
{
  val = "NO DATA YET";          //create a variable for printing when there is no data
  size(600,200);               // defines the size of the window
  variable_font = createFont("Arial",40,true);             // defines the size of the used font
  String portName = Serial.list()[0];               //change the 0 to a 1 or 2 etc. to match the port
  myPort = new Serial(this,portName, 9600);           // initializes the serial port
  myPort.bufferUntil ( '\n' );             //Sets a specific byte to buffer until before calling serialEvent(), byte '\n'
}

void serialEvent(Serial myPort){              // checks if there is an event at the input of the serial port
 val = myPort.readStringUntil('\n');         // read it and store it in val
}
void draw()                   //continuously executes the lines of code contained inside this block until the program is stopped                           
{
fill(0, 12);                               // set the color of the rectangle
rect(10, 10, 580, 180, 25);               //draws a rectangle with the given coordinates
fill(255);                                // fills with white color
noStroke();                               // nothing will is drawn to the screen.
if (val!="NO DATA YET"){                  // checks if there is no input data yet
textSize(30);                             // defines the size of the text
text(val,30,100);   // STEP 5 Display Text
fill(0);                                // fills with black color
}
}
```
When working on this first part I had problems with the sensor readings, I had connected it to the Arduino but I had not included the library "Adafruit_SGP30.h" which is fundamental in order to operate with this sensor.

### Video

In addition to the previous explanations, pictures and provided code, here, I show a video which contains the results of the project with the Arduino.

<iframe src="https://player.vimeo.com/video/401851374" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


## Proposed setup and steps for the Attiny412.
For this second part I created a simple interface with Processing to interact with the Attiny412 through an Arduino UNO which was used as intermediate interface because I did not have serial adapter for the conection Computer-board. For this part, I used the board that I built in FABLAB (Attiny412), one Arduino UNO, one breadboard, one resitor (1k), 4 wires and the software Processing. The preocess of building the board, the datasheet, its structure, the programming procedure and other complementary information can be found [here](https://dechevar19.gitlab.io/gitlab-project/assignments/week09/). The PCB was programmed using the code that is displayed below with an Arduino UNO as programmer, which is an alternative when the recommended programmer is not available. Ports 2 and 3 of the PCB were connected to ports 0 and 1 of the Arduino for transmission and reception, respectively. Moreover, the PCB was fed from  VCC and GND ports of the Arduino. The Arduino only interfaces the communication between the computer and PCB and hence, was connected to the PC using the serial port. The interface created using processing only has one button,  when this button is clicked the application sends a byte (character '1') through the serial port, this one is received by the arduino and forwarded to the Attiny412, the Attiny412 checks the incomming byte and if it is the '1' increases a counter by 1, then, in the loop the parity of the counter is checked, if the counter is odd, the led of the PCB start flashing with a frequency determined by the delay introduced in the code. On the other hand, if the counter is even, the led is turned off. This is basically, the functioning of this project, the codes for that purpose are displayed below.
## Code for Arduino.
The code for the Arduino is shown below, it also can be found [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/arduino_interface.ino).

```
#include <SoftwareSerial.h> 
SoftwareSerial Serial(0, 1); // RX, TX 
void setup() {              
  Serial.begin(9600);         // Initializes the port to a baud rate of 9600
  pinMode(3, INPUT);            // pin 3 as input
  } 
 void loop() {}
```

## Code for Attiny412.
The code for the Attiny412 is shown below, it also can be found [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/attiny.ino).

```
#include <SoftwareSerial.h>
SoftwareSerial Serial(2, 3);        // RX, TX
const char state = '1';         // led off constant
const int pinled =  1;      // the number of the LED pin
int counter = 0;                //declares a counter
void setup() {                  // setup initialization
  mySerial.begin(9600);         // initliazes the serial port to 9600 baudios
  pinMode(pinled, OUTPUT);      // declares pin 1 as output
  pinMode(3, INPUT);            //declares pin 3 as input
}
void loop() {                             //main loop
  if (mySerial.available() > 0) {         // checks the availability of the serial port
    char receive = Serial.read();         // reads the incoming byte and assigns its value to 'receive'
    if (receive == state){                  // checks the value of the incoming byte
     counter = counter + 1;                 // increases the counter in 1
    }
  }
  if (counter%2 == 1){                    // if the counter has an odd value flashes the led
     digitalWrite(pinled, HIGH);          // turns on the led
     delay(250);                           //introduces a delay
     digitalWrite(pinled, LOW);             // turns off the led
     delay(250);                          //introduces a delay
  }
}
```

## Code for processing.
Finally the code for processing is shown below and can be found [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/attiny_proce.pde)

```
// GUI interface for the ATtiny412 
import processing.serial.*;              // import the required libraries
import controlP5.*;                      // offers a range of controllers that allows to easily change and adjust values
ControlP5 gui;
Serial serial_port;                       // Defines the variable serial_port
void setup(){
//set the window size
size(200,100);                            
noStroke();        //Disables drawing the stroke (outline)
//Create the new GUI
gui = new ControlP5(this);              
PFont GUIfont = createFont("arial", 20);
  gui.setFont(GUIfont);

gui.addButton("flash")                    // create a button called 'flash' and calls public void flash for executing the action.
   //Set the position of the button : (X,Y)
   .setPosition(50,25)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
   String port = Serial.list()[0];
   serial_port = new Serial(this, port, 9600);      //sets the serial port to 9600 baudios/s
}

public void flash(int value){
// activated by the "flash" Button
serial_port.write('1');                     // sends the data through the serial port
}
void draw(){}
```
This code basically does the same as the previous processing code, with the diference that this one sends data through serial port instead of reading the data. For this purpose at the beginning of the code all the variables are declared, and defined the features of the window that will be displayed. Then, the button and its features are declared, and determined the way in which the button action will take place, release the mouse button or press it. Finally, the window is drawn and mantained until the application is closed. During the execution of this project I faced problems such as the way of connection between the Arduino and PCB, the issue was that I was connecting transmission to transmission, but it was easily solved by interchanging the pins. The library ControlP5 offers a range of controllers that allow you to easily change and adjust values while your sketch is running. Each controller is identified by a unique name assigned when creating a controller. ControlP5 locates variables and functions inside your sketch and will link controllers to matching variables or functions automatically. The flash button is activated when is pressed (.activateBy(ControlP5.PRESS)), then, the function 'public void flash' is called and the action within it is executed.

## Processing interface.
Below, the created inteface with the button is displayed, the same button is used for both options, flash the led and turn it off.

![](../images/uploaded_images/interfaz.jpg)
### Video

In addition to the previous explanations, pictures and provided code, here, I show a video which contains the results of the project with the PCB.



<iframe src="https://player.vimeo.com/video/454135773" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


