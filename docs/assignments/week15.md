# 3. Applications and implications
The main goal this week was to propose a possible final project, a central idea and the possible tools and hardware to implement such project. In addition to this, it was required to explain the possible steps that must be taken for the implementation.

## Project : Annoying clock alarm with water.
The main idea behind this project is to build a clock alarm using arduino and other components, for making jokes to friends or family, pouring water over them when they are sleeping. For doing so, it is only required to connect a smartphone via bluetooth to the alarm clock and activate a timer which is displayed in a LCD, both the timer and the LCD will be controlled by the main board used for the project (Arduino UNO, alternatively, I could use a PCB with Attiny chips).
## Who has done what beforehand?.
Looking up on the Internet I found quite a lot of designs for funny or intelligent clock alarms. For example, in this link I found a design of a alarm clock for heavy sleepers using Arduino, where the alarm is activated with a annoying sound which can be only deactivated after answering correctly a set of questions. Quite similar to the one I propose, I found one where the main difference is that a buzzer is used for generating the sound. There are other designs of alarm clocks in the Internet, for example, I found another one using wake up lights. Here, I show some links to some similar projects and ideas.

[https://www.gadgetronicx.com/iq-alarm-clock-arduino/](https://www.gadgetronicx.com/iq-alarm-clock-arduino/)  

[https://www.instructables.com/id/The-Most-Annoying-Alarm-Clock-Ever/](https://www.instructables.com/id/The-Most-Annoying-Alarm-Clock-Ever/)  

[http://zeedijker.blogspot.com/2011/11/](http://zeedijker.blogspot.com/2011/11/)  

[https://create.arduino.cc/projecthub/smi1100/never-wake-up-too-late-alarm-clock-with-annoying-tone-38f16e](https://create.arduino.cc/projecthub/smi1100/never-wake-up-too-late-alarm-clock-with-annoying-tone-38f16e)  

[http://www.zonnepanelen.wouterlood.com/24-arduino-countdown-timer-with-two-displays-and-a-buzzer-part-1-design-and-construction-of-the-display-terminal/](http://www.zonnepanelen.wouterlood.com/24-arduino-countdown-timer-with-two-displays-and-a-buzzer-part-1-design-and-construction-of-the-display-terminal/)  

## Materials and components.
-1xArduino UNO.  
-1xLiquid crystal display (Practical IIC / I2C with 1602 Blue LCD Display Screen Board Module for Arduino).  
-1xStandard servo motor.  
-1xPower source.  
-1xVoltage regulator  
-1xBluetooth adapter for Arduino (HC-06 Bluetooth Module).  
-2x 330 ohm and 3x 10K resistors.
-1xSmartphone.  
-Female-Female and Male-Male jumper wires.  
-Male/Female headers.  
-Battery connectors.  
-Plywood.  
-Wire.  
-1xSmall disposable cup.  
Most of components and materials are plug and play, just need some soldering or jumping, with the exception of the plywood which needs to be cut in order to build the box, this process can be easiliy done in Fablab.
## Where will they come from? How much will they cost?
Most of the components can be found in any electronic store, however I can find the resistors, Arduino, connectors jumpers and wires in Fablab. The smartphone can be our own phone and the disposable cup can be found in any store. The price and place where I can buy all the other components are detailed below:  

[Bluetooth adapter](https://www.e-ville.com/fi/3045302-tarvikkeet-ja-varaosat-3d-tulostukseen/36315-hc-06-bluetooth-module.html)  8.95 €.  

[Liquid crystal display](https://www.gearbest.com/other-accessories/pp_216639.html?lkid=11408229)  6.25 €.  

[Servo motor](https://www.hobbylinna.fi/tuote/sg90-mikroservo/KTSG90/? gclid=CjwKCAjwtNf6BRAwEiwAkt6UQoDLXl6BPytgUQHYSw-yp74WeU5ksu8-mSlx97d8I6JUnWQi2mMbVxoC5koQAvD_BwE)  2.99 €.  

[Plywood](https://www.supermart.com/plywood-sheet-12x12x-125/?utm_source=googleshopping&utm_medium=cse&gclid=CjwKCAjwtNf6BRAwEiwAkt6UQg5kpHk9S0GHHUvLjMMfCsH0X26LJbdxn-r9-Nq03Qj13goBnMxKEBoCTEgQAvD_BwE)  6.71 €.  

[Resistors](https://docs.google.com/spreadsheets/u/0/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html)  0.01 €.  

[Arduino UNO](https://docs.google.com/spreadsheets/u/0/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html)  24.95 €.  

[7805 Voltage regulator](https://www.ebay.com/sch/i.html?_nkw=7805%20Voltage%20Regulator&norover=1&mkevt=1&mkrid=711-156598-662049-2&mkcid=2&keyword=7805%20voltage%20regulator&crlp=436352203123_&MT_ID=585546&geo_id=10232&rlsatarget=kwd-296759742360&adpos=&device=c&mktype=&loc=1005766&poi=&abcId=1141776&cmpgn=6524607973&sitelnk=&adgroupid=77244829334&network=g&matchtype=p&gclid=Cj0KCQjwwOz6BRCgARIsAKEG4FWpWXY7PL-Fv55p5I7o1yYXKhrJMeX4MFwT9fYpSqDYIZoj4uLx8k0aAlV0EALw_wcB)  0.63 €.  

[5V Power source](https://fi.computer-world.pro/lithium-battery-asus-x451m-x451mavvx327h-p-85874.html?gclid=Cj0KCQjwwOz6BRCgARIsAKEG4FWhMhwcDPBOgCBMQ8p43F4lI122H3F30hLd-zKopf5WklSOZ2uHFMEaAt6NEALw_wcB)   6.86 €.  

 The total cost of all these componets is around 57.35 euros without including the shipping price.
 
## What will it do? Main ideas of the project and main procedures.
The main idea of the project is the control of a servo motor using Arduino or alternatively, the PCB. The motor will hold a kind of arm, which simultaneously will have in the top corner a disposable cup with water, placed vertically. The arduino will also have a LCD where I will display a countdown in seconds, and also will have a bluetooth interface which I will use to connect to our smartphone and activate the countdown using a simple Android application. There must be a power source in order to feed the arduino and the servo, with a voltage regulator if the power provided by the source is not the required one by the Arduino and servo motor. For the electronic design I could use jumpers or soldering, it depends on one's own interests.  A simple schematic view of the project is shown below.

![](../images/uploaded_images/design.jpg)

I will need to build a wooden box to place all the electronics inside, then I will need to set the suitable sizes and cut the plywood. I will also need to leave a rectangular aperture for placing the display so that it can be seen from outside. The servo motor would be placed on top of the box with the cup of water. I will need to create an Arduino code for controlling the bluetooth inteface, the display and the servo. The functioning would be as follows:  

-I create an Android application that would have the option of sending the time up to the moment when the clock alarm is activated, for example if I want to activate the mechanism in 1 hour and a half, I would type the number 90 in the application and push the option 'start', this option would send the information via bluetooth to the arduino, which would start the countdown, such countdown would be displayed in the LCD. The code in the arduino has to be able to read the data from the bluetooth inteface and initialize a counter in minutes, this counter would start decreasing right away. When the countdown reach the zero value the Arduino sends the order to rotate 90 degree its shaft, and hence the water will be poured on the selected person. The countdown can also be seen in the Android application, either by receiving feedback from the Arduino or for activating its own countdown. Other options such as a fake deactivation button, can be used, this is just in case that the person gets up, if so, he/she could see the button saying 'deactivate' and push it, when the real function of the button is stop the countdown and pour the water right away, this way I can guarantee no escape from the 'trap' :).
## What parts will be made? What will I design?
For this design, I need to build the containing box, for this purpose I need to define the size of each of the pieces used for building the box. I will also need to solder the electronics (resistors, wires and connectors of every component), moreover, the code for the Arduino has to be created as well as the code in Java for creating the Android application for our smartphone. Other alternative in this project is the use of a PCB instead of Arduino, in this case I recommend the use of Attiny412, whose fabrication and programming processes can be found [here](https://dechevar19.gitlab.io/gitlab-project/assignments/week09/).
## What processes will be used?
The required processes are shown below:  
-CAD software(Eagle)(Only for the case that I use a PCB instead of Arduino). Milling machine will be used in this alternative case.  
-Laser cutting of the box components.  
-Electronics design.  
-Embedded programmimg. For the Arduino (or PCB).  
-Programming of an Android application.
##  What questions need to be answered?
I think from the design point of view, it would be advisable to determine if the design is cheaper to implement using the Arduino or designing the PCB. For all these tasks is required to be familiar with embedded programming (c++), to have knowledge of other languages such as JAVA (creating the Android application) and to have some basis of electronic. Other questions could arise here, for instance, is there any market for designs like this one? Is it profitable to do this design by ourselves or is it better to buy other existing prototypes in the market with other functionalities.?  
## How will it be evaluated?
The best way of evaluating this design would be of course trying to wake someone up, but of course it might fail, hence, I would test it as many times as possible so that I can evaluate how reliable my design is, this way I could know how long the battery would provide the required energy for the whole process and also know how many times the alarm was succesfull in for instance, 20 trials. Some components can be evaluated also separatedly before implementing the project, for example, the Arduino (or PCB), LCD, power source, etc, so that I can check if there is any malfunctioning on any of them.
## Conclusions.
These are the main ideas behind the project, I do not show real pictures or measures since the project was conceived in a moment where the materials and tools were difficult to acquire. Nevertheless, it can be implemented using part of the knowledge gained during the course Embedded systems, since it is required to develop visual applications, wireless communications and control of external devices.