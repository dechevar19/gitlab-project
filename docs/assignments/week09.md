# 1. Embedded programming. Running a simple application with Olimex. Bulding and programming a board with chip ATtiny 412.

## Running a simple application with Olimex.
The goal of this task was to run a simple program in the board, such that it does something, with as many different programming languages and programming environments as possible. I used for this assignment Olimex board whose processor is ATMega128 as shown in the image below and the arduino IDE for programming the board.  

## Board datasheet.
AVR-MT128 is simple but powerful board which uses the MCU ATMega128 from Atmel. With its LCD, buttons, relay and variety of interfaces such as
RS232 (in two variants – 4 pins and DB9), JTAG, ISCP, Dallas, etc. this board is suitable for different embedded systems applications. 

## Useful links
https://camo.githubusercontent.com/2e9fe11875f8ca873e6d9ed5c9602acdaf5c089f/68747470733a2f2f692e696d6775722e636f6d2f737765524a73332e6a7067
https://www.olimex.com/Products/AVR/Development/AVR-MT128/resources/AVR-MT128-SCH-REV-A.pdf
## Followed steps.

The detailed steps are explained below:
Firt I connected the board to the computer using the provided interface (AVR-JTAG-USB), then I opened the Arduino IDE in the PC and configured the IDE as follows
 From Tools>Boards select: ATmega128 b. From Tools>Bootloader select: No Bootloader c. From Tools>Clock select: External 16 MHz d. From Tools>BOD select: BOD Disabled e. From Tools>Compiler LTO: select: LTO Disabled f. Connect JTAG and Olimex board.
 In addition to this steps the configuration of the COM port was also required.
 The image below shows the Arduino environment
 
 ![](../images/uploaded_images/arduino_environment1.jpg)
 
 and the code used is also shown  below

 

## Code used for this week

Use the three backticks to separate code.

```
// include the library code:
#include <LiquidCrystal.h>
// initialize the library by associating any needed LCD interface pin
int pinSwitch = 44;
int pinled = 38;
int pinSwitch_2 =40;

void setup() {
//Pin29 is R/W pin for LCD
  pinMode(29, OUTPUT);
  digitalWrite(29, LOW);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
 
}
void loop() {
  
  if (digitalRead(pinSwitch)==LOW){
  
    lcd.print("Blinking led");
    
    while (digitalRead(pinSwitch_2)==HIGH){
      
    digitalWrite(pinled, HIGH);
    
    delay(200);
    
    digitalWrite(pinled, LOW);
    
    delay(200);
    
    }
    
    }
  else
  {
  lcd.clear();
  digitalWrite(pinled, LOW);
  }
}
```
This code basically prints the message 'Blinking led' when the upper buttom is pressed and the led starts blinking with a period of 200 ms, the 'blinking' is stopped by pressing the lower buttom. This can be seen in the video attached below in the page. It is a basic program yet but for the next weeks the complexity will increse since the assigments will be more difficult. 
## Gallery

![](../images/uploaded_images/Processor.jpg)
![](../images/uploaded_images/olimex.jpg)


### From Vimeo

<iframe src="https://player.vimeo.com/video/401405144" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


## Bulding and programming a board with chip ATtiny 412.

The goal of this task was to build a programmable board using the chip ATtiny 412 and run a simple application on it.

## Materials and tools.
For this purpose I required the use of a chip ATtiny 412 (datasheet http://ww1.microchip.com/downloads/en/DeviceDoc/40001931A.pdf), 3 resistors (1k, 10k, 4.7k), one push button, one led, 2 capacitors (1 microF, 10 microF ), one 3-pin female connector and one 6-pin male connector (FTDI-SMD-HEADER). It was also required an Arduino ONE and the softwares Arduino IDE and Eagle. A zip file containing the eagle files as well as the .rml files for the milling machine can be found [here](https://gitlab.com/dechevar19/gitlab-project/-/blob/master/docs/images/uploaded_images/PCB.zip). 

## Followed steps
The first step was the design of the board (printed circuit), for this task I used the software Eagle which is a Tool for designing PCBs and diagrams. With this software I got 2 .png files containig schematic view of the PCB (TOP and OUTLINE). These two files were used to generate the .rml files (webpage used to generate the .rml files https://mods.cba.mti.edu) which are used in the milling machine. Below the TOP.png file obtained with Eagle is displayed.

![](../images/uploaded_images/TOP.png)

Once both files (.rml) are obtained from the previously mentioned webpage, the next step is to upload them to the software used for the milling machine (VPanel for SRM-20) and proceed to cut the PCB. Images of the software and the machine are displayed below.

![](../images/uploaded_images/3.png)
![](../images/uploaded_images/2.png)

Then, the files obtained from Eagle are uploaded to the software, which controls the milling process, the 3D position of the small drill used for the process is adjusted using the buttons x, y and z. Also the cursor step can be defined when defining the position. 

Bellow a smal video showing the milling process is shown.
<iframe src="https://player.vimeo.com/video/436754523" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

After cutting the PCB the next step is to solder the components on the board, the following image shows the final result.

![](../images/uploaded_images/board.png)

Up to this point, the board has been built, now it is time to test it with a simple program. In this case the led of the board will blink every 500 ms and if the push button is pressed it will blink with a shorter period of 100 ms. In order to do that we need an interface that allows us to program the board. An easy way of doing it is to convert an Arduino UNO to a programming interface. The steps for doing so can be found in the following link: https://github.com/SpenceKonde/megaTinyCore/blob/master/MakeUPDIProgrammer.md.
Below we show the circuit and connections for the programming interface, the picture shows the configuration with an Arduino NANO but with Arduino UNO can be done as well using the same pins. Notice that the capacitor is optional in this configuration.

![](../images/uploaded_images/interface.png)

The code for the program is shown below, the board is programmed using the Arduino IDE.


```
const int Pin_button = 0;     // the number of the pushbutton pin
const int Pin_led =  1;      // the number of the LED pin

// variables will change:
int buttonState = 0;         // variable for reading the pushbutton status

void setup() {
  // initialize the LED pin as an output:
  pinMode(Pin_led, OUTPUT);
  // initialize the pushbutton pin as an input:
  pinMode(Pin_button, INPUT);
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(Pin_button);

  // check if the pushbutton is pressed. If it is, the buttonState is LOW (pull-up resistor):
  if (buttonState == LOW) {
    // turn LED on:
    digitalWrite(Pin_led, HIGH);
    delay(100);
    digitalWrite(Pin_led, LOW);
    delay(100);
  } else {
    // turn LED off:
     digitalWrite(Pin_led, HIGH);
    delay(500);
    digitalWrite(Pin_led, LOW);
    delay(500);
  }
}
```
After uploading the code to the board the functioning was checked. The following video shows the code, the connections and the board doing the intended operations. After doing all the steps it was possible to implement a simple code in a PCB using Attiny 412, which was build from scratch. Other more complicated applicatios can be implemented using this board, serial communications, interface control, etc.

<iframe src="https://player.vimeo.com/video/436767174" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
