#include <Wire.h>            // Including the required libraries
#include "Adafruit_SGP30.h"   // Including the required libraries

Adafruit_SGP30 sgp;           // 
int ledPin = 13;              // Declare the variable ledpin as an integer and assign pin 13

void setup() {
Serial.begin(9600);             //Initialize the serial port to 9600 baudios
sgp.begin();                    // Initialize the sensor
pinMode(ledPin, OUTPUT);        // Declare the pin 13 as output
}

void loop() {                   // Loop
sgp.IAQmeasure();               // Reads the data from the sensor
if (sgp.eCO2 >= 2000){          // compares the reading to the threshold
digitalWrite(ledPin, HIGH);     //Turns on the led
Serial.print("Dangerous level of CO2 (ppm): "); Serial.println(sgp.eCO2);   // sends the message through the serial port
delay(1000);                                // Introduces  1s delay between readings

}
else{
Serial.print("Normal level of CO2 (ppm): "); Serial.println(sgp.eCO2);     // sends the message through the serial port
delay(1000);     // Introduces  1s delay between readings
digitalWrite(ledPin, LOW);    //Turns off the led
}
}
