import processing.serial.*;
Serial myPort;  // Create object from Serial class
String val;     // Data received from the serial port
PFont variable_font; 

void setup()
{
  val = "NO DATA YET";          //create a variable for printing when there is no data
  size(600,200);               // defines the size of the window
  variable_font = createFont("Arial",40,true);             // defines the size of the used font
  String portName = Serial.list()[0];               //change the 0 to a 1 or 2 etc. to match the port
  myPort = new Serial(this,portName, 9600);           // initializes the serial port
  myPort.bufferUntil ( '\n' );             //Sets a specific byte to buffer until before calling serialEvent(), byte '\n'
}

void serialEvent(Serial myPort){              // checks if there is an event at the input of the serial port
 val = myPort.readStringUntil('\n');         // read it and store it in val
}
void draw()                   //continuously executes the lines of code contained inside this block until the program is stopped                           
{
fill(0, 12);                               // set the color of the rectangle
rect(10, 10, 580, 180, 25);               //draws a rectangle with the given coordinates
fill(255);                                // fills with white color
noStroke();                               // nothing will is drawn to the screen.
if (val!="NO DATA YET"){                  // checks if there is no input data yet
textSize(30);                             // defines the size of the text
text(val,30,100);   // STEP 5 Display Text
fill(0);                                // fills with black color
}
}
