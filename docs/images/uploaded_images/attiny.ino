#include <SoftwareSerial.h>
SoftwareSerial mySerial(2, 3);        // RX, TX
const char state = '1';         // led off constant
const int pinled =  1;      // the number of the LED pin
int counter = 0;                //declares a counter
void setup() {                  // setup initialization
  mySerial.begin(9600);         // initliazes the serial port to 9600 baudios
  pinMode(pinled, OUTPUT);      // declares pin 1 as output
  pinMode(3, INPUT);            //declares pin 3 as input
}
void loop() {                             //main loop
  if (mySerial.available() > 0) {         // checks the availability of the serial port
    char receive = mySerial.read();         // reads the incoming byte and assigns its value to 'receive'
    if (receive == state){                  // checks the value of the incoming byte
     counter = counter + 1;                 // increases the counter in 1
    }
  }
  if (counter%2 == 1){                    // if the counter has an odd value flashes the led
     digitalWrite(pinled, HIGH);          // turns on the led
     delay(250);                           //introduces a delay
     digitalWrite(pinled, LOW);             // turns off the led
     delay(250);                          //introduces a delay
  }
}
