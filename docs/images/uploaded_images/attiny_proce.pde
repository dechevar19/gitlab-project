// GUI interface for the ATtiny412 serial communication.
import processing.serial.*;              // import the required libraries
import controlP5.*;
ControlP5 gui;
Serial serial_port;
void setup(){
//set the window size
size(200,100);
noStroke();
//Create the new GUI
gui = new ControlP5(this);
PFont GUIfont = createFont("arial", 20);
  gui.setFont(GUIfont);

gui.addButton("flash")                    // create a button called 'flash'
   //Set the position of the button : (X,Y)
   .setPosition(50,25)
   //Set the size of the button : (X,Y)
   .setSize(100,50)
   //Set the pre-defined Value of the button : (int)
   .setValue(0)
   //set the way it is activated : RELEASE the mouseboutton or PRESS it
   .activateBy(ControlP5.PRESS);
   String port = Serial.list()[0];
   serial_port = new Serial(this, port, 9600);
}

public void flash(int value){
// activated by the "OFF" Button and print the current led's state
serial_port.write('1');
}
public void controlEvent(ControlEvent theEvent) {}
void draw(){}
